import bs4
import sys
import re
from PyQt5.QtWebEngineWidgets import QWebEnginePage
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QUrl


def extract_temp_from_page_text(page_soup):
    try:
        val = page_soup.find(id="ajaxtemp")
        m = re.findall(r" *(-?\d+\.\d).C *", val.contents[0], re.MULTILINE)
        return "%s degrees Celsius" % round(float(m[0]))
    except:
        raise SystemExit('ERROR: Temp not found')


class DynamicPage(QWebEnginePage):
    def __init__(self, url):
        self.app = QApplication(sys.argv)
        QWebEnginePage.__init__(self)
        self.html = ''
        self.loadFinished.connect(self._on_load_finished)
        self.load(QUrl(url))
        self.app.exec_()

    def _on_load_finished(self):
        self.html = self.toHtml(self.callable)

    def callable(self, html_str):
        self.html = html_str
        self.app.quit()


def main():
    # url = "https://www.weerindelft.nl/"
    url = "https://weerindelft.nl/WU/55ajax-dashboard-testpage.php"

    page = DynamicPage(url)
    soup = bs4.BeautifulSoup(page.html, 'html.parser')

    print(extract_temp_from_page_text(soup))


if __name__ == '__main__':
    main()
